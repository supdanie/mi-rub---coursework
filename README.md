# Reservation system for sport centre

# Installation and creating an user for using the reservation system for a sport center

You must clone the sources from this repository by this command 'git clone https://gitlab.fit.cvut.cz/supdanie/mi-rub---coursework.git'. Then you have to go to the "mi-rub---coursework" directory by the command 'cd mi-rub---coursework'.

In the directory "mi-rub---coursework" you must run the command "bundle install". After installing gems you must run the command 'yarn install --check-files'. After installing files by yarn you must migrate database by the command 'rails db:migrate'.

Then you have to register to the application. After the registration you should update the file development.sqlite file.

For example you register as a user with these information:

username: root

email: root@centrum.cz

password: rootroot

You should update the value of the is_admin attribute to value 1 for the user with username root. You can make the root user admin by SQL query: 'UPDATE users SET is_admin = 1 WHERE username="root"'. You can also do it for example in DB browser for SQLite or in VSCode by a SQLite plugin.

Of course, you can make more users by multiple registration. During each successful registration you create one user.

After registration of a user you can log in as the user with the filled credentials (username or e-mail address and password) (in this case root or root@centrum.cz and rootroot).

# Usage as an admin

Before adding a reservation you must add a hall. You can create a hall by clicking on "Halls" item in the top menu, following clicking on the "New" button and filling the form for adding a hall. You need not to add a description, but other information are mandatory.

When you are logged in as the admin, you can add time range by clicking on "Time ranges" item in the top menu, following clicking on the "New" button and filling the form. This step is not neccessary, but you should do it as the admin, if you want to specify different prices for different time ranges.

Admin also can add a price advantage by clicking on "Price advantages" item, following click on the "New" button and filling the form.

By similar way (click on the item "Price specifications" in the top menu) the admin can add a price specification for any hall and time range. For example, admin can specify the price of 150 CZK per hour to the hall with the title "Tenisová hala" for the added time range (8:00 - 12:00). Admin must do it before specifying a discount for any price advantage.

By similar way (click on the item "Discounts for price advantages" in the top menu) the admin can add a specification for price advantage. For example, admin can specify that users who have price advantage "Kredit 5000" have discount 30% to the hall with the title "Tenisová hala" for the time range from 8:00 to 12:00 (added price specification from the previous example).

# Usage as any user

Any registered and logged user can make a reservation of any hall. The user can click on the item "Halls" and select a hall to show by clicking the "Show" button and then clicking on the button "Make Reservation". The user can fill the date and the time of the reservation.

The second way that user can make a reservation, is click on the link "My reservations" in the top menu and clicking the "New" button. The user can fill the date and the time of the reservation and select the hall. By clicking on the link "My reservations" user can show his/her reservations.

