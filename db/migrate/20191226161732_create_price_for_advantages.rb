# frozen_string_literal: true

class CreatePriceForAdvantages < ActiveRecord::Migration[6.0]
  def change
    create_table :price_for_advantages do |t|
      t.integer :discount
      t.references :price_advantage, null: false, foreign_key: true
      t.references :price_for_reservation, null: false, foreign_key: true

      t.timestamps
    end
  end
end
