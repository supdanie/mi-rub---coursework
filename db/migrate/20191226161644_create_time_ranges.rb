# frozen_string_literal: true

class CreateTimeRanges < ActiveRecord::Migration[6.0]
  def change
    create_table :time_ranges do |t|
      t.time :from
      t.time :to

      t.timestamps
    end
  end
end
