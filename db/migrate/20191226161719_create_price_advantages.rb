# frozen_string_literal: true

class CreatePriceAdvantages < ActiveRecord::Migration[6.0]
  def change
    create_table :price_advantages do |t|
      t.string :title
      t.integer :order

      t.timestamps
    end
  end
end
