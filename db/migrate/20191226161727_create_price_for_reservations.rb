# frozen_string_literal: true

class CreatePriceForReservations < ActiveRecord::Migration[6.0]
  def change
    create_table :price_for_reservations do |t|
      t.integer :price
      t.references :hall, null: false, foreign_key: true
      t.references :time_range, null: false, foreign_key: true

      t.timestamps
    end
  end
end
