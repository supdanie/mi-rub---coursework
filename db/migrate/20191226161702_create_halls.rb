# frozen_string_literal: true

class CreateHalls < ActiveRecord::Migration[6.0]
  def change
    create_table :halls do |t|
      t.string :title
      t.time :open_from_working_days
      t.time :open_to_working_days
      t.time :open_from_weekends
      t.time :open_to_weekends
      t.text :description
      t.integer :default_price_for_reservation
      t.string :sport

      t.timestamps
    end
  end
end
