# frozen_string_literal: true

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20_191_226_161_732) do
  create_table 'halls', force: :cascade do |t|
    t.string 'title'
    t.time 'open_from_working_days'
    t.time 'open_to_working_days'
    t.time 'open_from_weekends'
    t.time 'open_to_weekends'
    t.text 'description'
    t.integer 'default_price_for_reservation'
    t.string 'sport'
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
  end

  create_table 'price_advantages', force: :cascade do |t|
    t.string 'title'
    t.integer 'order'
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
  end

  create_table 'price_for_advantages', force: :cascade do |t|
    t.integer 'discount'
    t.integer 'price_advantage_id', null: false
    t.integer 'price_for_reservation_id', null: false
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
    t.index ['price_advantage_id'], name: 'index_price_for_advantages_on_price_advantage_id'
    t.index ['price_for_reservation_id'], name: 'index_price_for_advantages_on_price_for_reservation_id'
  end

  create_table 'price_for_reservations', force: :cascade do |t|
    t.integer 'price'
    t.integer 'hall_id', null: false
    t.integer 'time_range_id', null: false
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
    t.index ['hall_id'], name: 'index_price_for_reservations_on_hall_id'
    t.index ['time_range_id'], name: 'index_price_for_reservations_on_time_range_id'
  end

  create_table 'reservations', force: :cascade do |t|
    t.date 'date'
    t.time 'from'
    t.time 'to'
    t.string 'sport'
    t.integer 'user_id', null: false
    t.integer 'hall_id', null: false
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
    t.index ['hall_id'], name: 'index_reservations_on_hall_id'
    t.index ['user_id'], name: 'index_reservations_on_user_id'
  end

  create_table 'time_ranges', force: :cascade do |t|
    t.time 'from'
    t.time 'to'
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
  end

  create_table 'users', force: :cascade do |t|
    t.string 'email', default: '', null: false
    t.string 'encrypted_password', default: '', null: false
    t.string 'reset_password_token'
    t.datetime 'reset_password_sent_at'
    t.datetime 'remember_created_at'
    t.integer 'price_advantage_id'
    t.string 'phone', default: '', null: false
    t.string 'username', default: '', null: false
    t.string 'name', default: '', null: false
    t.string 'surname', default: '', null: false
    t.date 'birthday'
    t.boolean 'is_admin', default: false, null: false
    t.datetime 'created_at', precision: 6, null: false
    t.datetime 'updated_at', precision: 6, null: false
    t.index ['email'], name: 'index_users_on_email', unique: true
    t.index ['price_advantage_id'], name: 'index_users_on_price_advantage_id'
    t.index ['reset_password_token'], name: 'index_users_on_reset_password_token', unique: true
  end

  add_foreign_key 'price_for_advantages', 'price_advantages'
  add_foreign_key 'price_for_advantages', 'price_for_reservations'
  add_foreign_key 'price_for_reservations', 'halls'
  add_foreign_key 'price_for_reservations', 'time_ranges'
  add_foreign_key 'reservations', 'halls'
  add_foreign_key 'reservations', 'users'
  add_foreign_key 'users', 'price_advantages'
end
