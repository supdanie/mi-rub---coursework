# frozen_string_literal: true

# This module is the application helper with method for formating time
# and getting the e-mail address of the given user.
module ApplicationHelper
  def format_time(time)
    time_hour = time.hour.to_s.length == 1 ? "0#{time.hour}" : time.hour.to_s
    time_min = time.min.to_s.length == 1 ? "0#{time.min}" : time.min.to_s
    "#{time_hour}:#{time_min}"
  end

  def date_to_s(time)
    "#{time.day}.#{time.month}.#{time.year}"
  end

  def email_of_user(id)
    user = User.find(id)
    user.email
  end
end
