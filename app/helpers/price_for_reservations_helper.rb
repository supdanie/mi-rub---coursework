# frozen_string_literal: true

# This helper is used for getting the string with the information about
# the price specification given by the identificator.
module PriceForReservationsHelper
  def info_of_price_for_reservation(id)
    price_specification = PriceForReservation.find(id)
    hall = Hall.find(price_specification.hall_id)
    time_range_to_string = time_range_string(price_specification.time_range_id)

    "#{price_specification.price} CZK/hour, #{hall.title} for time range #{time_range_to_string}"
  end
end
