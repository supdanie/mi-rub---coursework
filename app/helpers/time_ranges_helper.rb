# frozen_string_literal: true

# This helper is used for getting the string for the given identificator of a time range.
module TimeRangesHelper
  def time_range_string(id)
    time_range = TimeRange.find(id)
    "#{format_time(time_range.from)} - #{format_time(time_range.to)}"
  end
end
