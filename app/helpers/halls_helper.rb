# frozen_string_literal: true

# This helper is used for finding out the title of the hall given by the identificator.
module HallsHelper
  def title_of_hall(id)
    hall = Hall.find(id)
    hall.title
  end
end
