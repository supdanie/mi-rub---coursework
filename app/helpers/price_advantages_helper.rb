# frozen_string_literal: true

# This helper is used for getting the title of the price advantage given by the identificator.
module PriceAdvantagesHelper
  def title_of_price_advantage(id)
    price_advantage = PriceAdvantage.find(id)
    price_advantage.title
  end
end
