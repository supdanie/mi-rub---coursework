# frozen_string_literal: true

json.array! @price_for_advantages, partial: 'price_for_advantages/price_for_advantage', as: :price_for_advantage
