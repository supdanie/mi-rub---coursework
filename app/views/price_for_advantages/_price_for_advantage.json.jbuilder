# frozen_string_literal: true

json.extract! price_for_advantage, :id, :discount, :price_advantage_id, :price_for_reservation_id, :created_at, :updated_at
json.url price_for_advantage_url(price_for_advantage, format: :json)
