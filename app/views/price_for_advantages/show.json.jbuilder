# frozen_string_literal: true

json.partial! 'price_for_advantages/price_for_advantage', price_for_advantage: @price_for_advantage
