# frozen_string_literal: true

json.partial! 'price_for_reservations/price_for_reservation', price_for_reservation: @price_for_reservation
