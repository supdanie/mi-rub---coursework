# frozen_string_literal: true

json.array! @price_for_reservations, partial: 'price_for_reservations/price_for_reservation', as: :price_for_reservation
