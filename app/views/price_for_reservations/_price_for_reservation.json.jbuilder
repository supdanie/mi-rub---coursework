# frozen_string_literal: true

json.extract! price_for_reservation, :id, :price, :hall_id, :time_range_id, :created_at, :updated_at
json.url price_for_reservation_url(price_for_reservation, format: :json)
