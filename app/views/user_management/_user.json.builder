# frozen_string_literal: true

json.extract! user, :email, :username, :name, :surname, :birthday, :price_advantage_id
json.url reservation_url(user, format: :json)
