# frozen_string_literal: true

json.extract! reservation, :id, :date, :from, :to, :user_id, :hall_id, :created_at, :updated_at
json.url reservation_url(reservation, format: :json)
