# frozen_string_literal: true

json.array! @time_ranges, partial: 'time_ranges/time_range', as: :time_range
