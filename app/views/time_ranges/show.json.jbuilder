# frozen_string_literal: true

json.partial! 'time_ranges/time_range', time_range: @time_range
