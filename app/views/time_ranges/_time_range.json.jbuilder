# frozen_string_literal: true

json.extract! time_range, :id, :from, :to, :created_at, :updated_at
json.url time_range_url(time_range, format: :json)
