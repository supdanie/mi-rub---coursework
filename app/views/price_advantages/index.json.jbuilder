# frozen_string_literal: true

json.array! @price_advantages, partial: 'price_advantages/price_advantage', as: :price_advantage
