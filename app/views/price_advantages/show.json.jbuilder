# frozen_string_literal: true

json.partial! 'price_advantages/price_advantage', price_advantage: @price_advantage
