# frozen_string_literal: true

json.extract! price_advantage, :id, :title, :order, :created_at, :updated_at
json.url price_advantage_url(price_advantage, format: :json)
