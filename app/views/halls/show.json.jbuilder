# frozen_string_literal: true

json.partial! 'halls/hall', hall: @hall
