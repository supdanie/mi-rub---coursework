# frozen_string_literal: true

json.array! @halls, partial: 'halls/hall', as: :hall
