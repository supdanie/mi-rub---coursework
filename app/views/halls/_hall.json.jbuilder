# frozen_string_literal: true

json.extract! hall, :id, :title, :open_from_working_days, :open_to_working_days, :open_from_weekends, :open_to_weekends, :default_price_for_reservation, :sport, :description, :created_at, :updated_at
json.url hall_url(hall, format: :json)
