# frozen_string_literal: true

json.extract! reservation, :id, :date, :from, :to
json.url hall_url(hall, format: :json)
