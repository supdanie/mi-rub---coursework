# frozen_string_literal: true

# This class is a controller for management of specifications of discounts for
# a combination of price advantage and price specification. Price specification
# contains a hall and a time range and price for the given hall and time range.
class PriceForAdvantagesController < ApplicationController
  load_and_authorize_resource
  before_action :set_price_for_advantage, only: %i[show edit update destroy]
  before_action :set_price_advantage_array, only: %i[new edit create update]
  before_action :set_price_for_reservation_array, only: %i[new edit create update]

  # GET /price_for_advantages
  # GET /price_for_advantages.json
  def index
    @price_for_advantages = PriceForAdvantage.all
  end

  # GET /price_for_advantages/1
  # GET /price_for_advantages/1.json
  def show; end

  # GET /price_for_advantages/new
  def new
    @price_for_advantage = PriceForAdvantage.new
  end

  # GET /price_for_advantages/1/edit
  def edit; end

  # POST /price_for_advantages
  # POST /price_for_advantages.json
  def create
    @price_for_advantage = PriceForAdvantage.new(price_for_advantage_params)

    respond_to do |format|
      if @price_for_advantage.save
        notice = 'Price for advantage was successfully created.'
        format.html { redirect_to @price_for_advantage, notice: notice }
        format.json { render :show, status: :created, location: @price_for_advantage }
      else
        format.html { render :new }
        format.json { render json: @price_for_advantage.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /price_for_advantages/1
  # PATCH/PUT /price_for_advantages/1.json
  def update
    respond_to do |format|
      if @price_for_advantage.update(price_for_advantage_params)
        notice = 'Price for advantage was successfully updated.'
        format.html { redirect_to @price_for_advantage, notice: notice }
        format.json { render :show, status: :ok, location: @price_for_advantage }
      else
        format.html { render :edit }
        format.json { render json: @price_for_advantage.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /price_for_advantages/1
  # DELETE /price_for_advantages/1.json
  def destroy
    @price_for_advantage.destroy
    respond_to do |format|
      format.html { redirect_to price_for_advantages_url, notice: 'Price for advantage was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def set_price_advantage_array
    @price_advantages = PriceAdvantage.all
    @price_advantage_array = []
    @price_advantages.each do |price_advantage|
      @price_advantage_array.push([price_advantage.title, price_advantage.id])
    end
  end

  def set_price_for_reservation_array
    @price_for_reservations = PriceForReservation.all
    @price_for_reservation_array = []
    @price_for_reservations.each do |price_for_reservation|
      info = helpers.info_of_price_for_reservation(price_for_reservation.id)
      @price_for_reservation_array.push([info, price_for_reservation.id])
    end
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_price_for_advantage
    @price_for_advantage = PriceForAdvantage.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def price_for_advantage_params
    params.require(:price_for_advantage).permit(:discount, :price_advantage_id, :price_for_reservation_id)
  end
end
