# frozen_string_literal: true

# This class is a controller for management of price specifications for 
# a given combination of a hall and a time range.
class PriceForReservationsController < ApplicationController
  load_and_authorize_resource
  before_action :set_price_for_reservation, only: %i[show edit update destroy]
  before_action :set_hall_array, only: %i[new edit create update]
  before_action :set_time_range_array, only: %i[new edit create update]

  # GET /price_for_reservations
  # GET /price_for_reservations.json
  def index
    @price_for_reservations = PriceForReservation.all
  end

  # GET /price_for_reservations/1
  # GET /price_for_reservations/1.json
  def show; end

  # GET /price_for_reservations/new
  def new
    @price_for_reservation = PriceForReservation.new
  end

  # GET /price_for_reservations/1/edit
  def edit; end

  # POST /price_for_reservations
  # POST /price_for_reservations.json
  def create
    @price_for_reservation = PriceForReservation.new(price_for_reservation_params)

    respond_to do |format|
      if @price_for_reservation.save
        notice = 'Price for reservation was successfully created.'
        format.html { redirect_to @price_for_reservation, notice: notice }
        format.json { render :show, status: :created, location: @price_for_reservation }
      else
        format.html { render :new }
        format.json { render json: @price_for_reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /price_for_reservations/1
  # PATCH/PUT /price_for_reservations/1.json
  def update
    respond_to do |format|
      if @price_for_reservation.update(price_for_reservation_params)
        notice = 'Price for reservation was successfully updated.'
        format.html { redirect_to @price_for_reservation, notice: notice }
        format.json { render :show, status: :ok, location: @price_for_reservation }
      else
        format.html { render :edit }
        format.json { render json: @price_for_reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /price_for_reservations/1
  # DELETE /price_for_reservations/1.json
  def destroy
    @price_for_reservation.destroy
    respond_to do |format|
      notice = 'Price for reservation was successfully destroyed.'
      format.html { redirect_to price_for_reservations_url, notice: notice }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_price_for_reservation
    @price_for_reservation = PriceForReservation.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def price_for_reservation_params
    params.require(:price_for_reservation).permit(:price, :hall_id, :time_range_id)
  end

  def set_hall_array
    @halls = Hall.all
    @hall_array = []
    @halls.each do |hall|
      @hall_array.push([hall.title, hall.id])
    end
  end

  def set_time_range_array
    @time_ranges = TimeRange.all
    @time_range_array = []
    @time_ranges.each do |time_range|
      from = helpers.format_time(time_range.from)
      to = helpers.format_time(time_range.to)
      @time_range_array.push(["#{from} - #{to}", time_range.id])
    end
  end
end
