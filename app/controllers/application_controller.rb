# frozen_string_literal: true

# This class is the application controller which is the base controller for all controllers in
# the application. This controller ensures that user is authenticated before every action.
# Permitted parameters of the current users are configured here.
class ApplicationController < ActionController::Base
  before_action :authenticate_user!
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    added_attrs = %i[username email name surname birthday password password_confirmation remember_me]
    devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
    devise_parameter_sanitizer.permit :account_update, keys: added_attrs
  end
end
