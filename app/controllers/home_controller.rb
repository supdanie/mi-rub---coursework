# frozen_string_literal: true

# This class is a controller for the homepage (landing page).
class HomeController < ApplicationController
  def index; end
end
