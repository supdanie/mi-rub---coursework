# frozen_string_literal: true

# This class is a controller for pages for management of halls in a sport centre
# or reservation any of the halls.
class HallsController < ApplicationController
  load_and_authorize_resource
  before_action :set_hall, only: %i[show edit update destroy reservation_form reservations]
  before_action :set_reservation_from_params, only: %i[reservations]

  # GET /halls
  # GET /halls.json
  def index
    @halls = Hall.all
  end

  # GET /halls/1
  # GET /halls/1.json
  def show; end

  # GET /halls/new
  def new
    @hall = Hall.new
  end

  # GET /halls/1/edit
  def edit; end

  # POST /halls
  # POST /halls.json
  def create
    @hall = Hall.new(hall_params)

    respond_to do |format|
      if @hall.save
        format.html { redirect_to @hall, notice: 'Hall was successfully created.' }
        format.json { render :show, status: :created, location: @hall }
      else
        format.html { render :new }
        format.json { render json: @hall.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /halls/1
  # PATCH/PUT /halls/1.json
  def update
    respond_to do |format|
      if @hall.update(hall_params)
        format.html { redirect_to @hall, notice: 'Hall was successfully updated.' }
        format.json { render :show, status: :ok, location: @hall }
      else
        format.html { render :edit }
        format.json { render json: @hall.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /halls/1
  # DELETE /halls/1.json
  def destroy
    @hall.destroy
    respond_to do |format|
      format.html { redirect_to halls_url, notice: 'Hall was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def reservation_form
    @reservation = Reservation.new
    @reservation.user_id = current_user.id
    @reservation.hall_id = params[:id]
  end

  def reservations
    respond_to do |format|
      if @reservation.save
        format.html { redirect_to @reservation, notice: 'Reservation was successfully created.' }
        format.json { render :show, status: :created, location: @reservation }
      else
        format.html { render :reservation_form }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_hall
    @hall = Hall.find(params[:id])
  end

  def set_reservation_from_params
    @reservation = Reservation.new(reservation_params)
    @reservation.user_id = current_user.id
    @reservation.hall_id = params[:id]
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def hall_params
    params.require(:hall).permit(:title, :open_from_working_days, :open_to_working_days, :open_from_weekends, :open_to_weekends, :default_price_for_reservation, :sport, :description)
  end

  def reservation_params
    params.require(:reservation).permit(:date, :from, :to)
  end
end
