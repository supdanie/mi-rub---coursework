# frozen_string_literal: true

# This class is a controller for management of price advantages.
class PriceAdvantagesController < ApplicationController
  load_and_authorize_resource
  before_action :set_price_advantage, only: %i[show edit update destroy]

  # GET /price_advantages
  # GET /price_advantages.json
  def index
    @price_advantages = PriceAdvantage.all
  end

  # GET /price_advantages/1
  # GET /price_advantages/1.json
  def show; end

  # GET /price_advantages/new
  def new
    @price_advantage = PriceAdvantage.new
  end

  # GET /price_advantages/1/edit
  def edit; end

  # POST /price_advantages
  # POST /price_advantages.json
  def create
    @price_advantage = PriceAdvantage.new(price_advantage_params)

    respond_to do |format|
      if @price_advantage.save
        @price_advantage.change_orders_of_other_advantages(1)
        notice = 'Price advantage was successfully created.'
        format.html { redirect_to @price_advantage, notice: notice }
        format.json { render :show, status: :created, location: @price_advantage }
      else
        format.html { render :new }
        format.json { render json: @price_advantage.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /price_advantages/1
  # PATCH/PUT /price_advantages/1.json
  def update
    old_order = @price_advantage.order
    respond_to do |format|
      if @price_advantage.update(price_advantage_params)
        @price_advantage.change_orders_after_update(old_order)
        notice = 'Price advantage was successfully updated.'
        format.html { redirect_to @price_advantage, notice: notice }
        format.json { render :show, status: :ok, location: @price_advantage }
      else
        format.html { render :edit }
        format.json { render json: @price_advantage.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /price_advantages/1
  # DELETE /price_advantages/1.json
  def destroy
    @price_advantage.change_orders_of_other_advantages(-1)
    @price_advantage.destroy
    respond_to do |format|
      notice = 'Price advantage was successfully destroyed.'
      format.html { redirect_to price_advantages_url, notice: notice }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_price_advantage
    @price_advantage = PriceAdvantage.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def price_advantage_params
    params.require(:price_advantage).permit(:title, :order)
  end
end
