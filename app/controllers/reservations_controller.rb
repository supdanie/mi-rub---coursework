# frozen_string_literal: true

# This class is a controller for management of reservations.
class ReservationsController < ApplicationController
  load_and_authorize_resource
  before_action :set_reservation, only: %i[show edit update destroy]
  before_action :set_user_array, only: %i[new edit create update]
  before_action :set_hall_array, only: %i[new edit create update]

  # GET /reservations
  # GET /reservations.json
  def index
    @reservations = Reservation.all
  end

  # GET /reservations/1
  # GET /reservations/1.json
  def show; end

  # GET /reservations/new
  def new
    @reservation = Reservation.new
  end

  # GET /reservations/1/edit
  def edit; end

  # POST /reservations
  # POST /reservations.json
  def create
    @reservation = Reservation.new(reservation_params)
    respond_to do |format|
      if @reservation.save
        format.html { redirect_to @reservation, notice: 'Reservation was successfully created.' }
        format.json { render :show, status: :created, location: @reservation }
      else
        format.html { render :new }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reservations/1
  # PATCH/PUT /reservations/1.json
  def update
    respond_to do |format|
      if @reservation.update(reservation_params)
        format.html { redirect_to @reservation, notice: 'Reservation was successfully updated.' }
        format.json { render :show, status: :ok, location: @reservation }
      else
        format.html { render :edit }
        format.json { render json: @reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reservations/1
  # DELETE /reservations/1.json
  def destroy
    @reservation.destroy
    respond_to do |format|
      url_to_redirect = current_user.is_admin ? reservations_url : '/my_reservations'
      format.html { redirect_to url_to_redirect, notice: 'Reservation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def my_reservations
    @my_reservations = Reservation.where(user_id: current_user.id)
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_reservation
    @reservation = Reservation.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def reservation_params
    params_arr = params.require(:reservation).permit(:date, :from, :to, :user_id, :hall_id)
    params_arr['user_id'] = current_user.id if current_user.is_admin == false
    params_arr
  end

  def set_user_array
    @users = User.all
    @user_array = [[current_user.username, current_user.id]]
    @users.each do |user|
      next if user.id == current_user.id

      @user_array.push([user.username, user.id])
    end
  end

  def set_hall_array
    @halls = Hall.all
    @hall_array = []
    @halls.each do |hall|
      @hall_array.push([hall.title, hall.id])
    end
  end
end
