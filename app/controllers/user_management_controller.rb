# frozen_string_literal: true

# This class is a controller for user management (updating user, listing users).
class UserManagementController < ApplicationController
  before_action :set_user, only: %i[edit create]

  def index
    @users = User.all
  end

  def edit
    @price_advantages = PriceAdvantage.all
    @price_advantage_array = [['No price advantage', '']]
    @price_advantages.each do |price_advantage|
      @price_advantage_array.push([price_advantage.title, price_advantage.id])
    end
  end

  def create
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to '/user_management', notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:email, :username, :name, :surname, :birthday, :price_advantage_id)
  end
end
