# frozen_string_literal: true

# This class represents a price advantage and it validates the given information
# (for example filled information in the form). It also changes the order of
# other price advantages.
class PriceAdvantage < ApplicationRecord
  has_many :users, dependent: :nullify
  has_many :price_for_advantages, dependent: :destroy

  validates :title, presence: true
  validates :order, presence: true
  validate :positive_order

  def change_orders_of_other_advantages(by)
    return if order.nil?

    price_advantages = PriceAdvantage.all
    price_advantages.each do |price_advantage|
      next if price_advantage.order < order || price_advantage.id == id

      price_advantage.order = price_advantage.order + by
      price_advantage.save
    end
  end

  def positive_order
    return if order.nil?

    errors.add(:order, :error) if order <= 0
  end

  def change_orders_after_update(old_order)
    return if old_order == order

    increase_orders_after_update(old_order) if old_order > order
    decrease_orders_after_update(old_order) if old_order < order
  end

  def increase_orders_after_update(old_order)
    price_advantages = PriceAdvantage.all
    price_advantages.each do |price_advantage|
      if price_advantage.order > old_order || price_advantage.order < order || price_advantage.id == id
        next
      end

      price_advantage.order = price_advantage.order + 1
      price_advantage.save
    end
  end

  def decrease_orders_after_update(old_order)
    price_advantages = PriceAdvantage.all
    price_advantages.each do |price_advantage|
      if price_advantage.order > order || price_advantage.order < old_order || price_advantage.id == id
        next
      end

      price_advantage.order = price_advantage.order - 1
      price_advantage.save
    end
  end
end
