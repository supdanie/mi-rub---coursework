# frozen_string_literal: true

# This class represents a time range and it checks the given information
# (for example from the form). It is used also during the check whether
# the hour price for reservation was not defined for the given hall and
# time range.
class TimeRange < ApplicationRecord
  has_many :price_for_reservations, dependent: :destroy

  validates :from, presence: true
  validates :to, presence: true
  validate :correct_time_range
  validate :no_overlap_for_any_hall

  def correct_time_range
    return if from.nil? || to.nil?

    errors.add(:from, :error) unless from < to
    errors.add(:to, :error) unless from < to
  end

  def no_overlap_for_any_hall
    overlap = 0
    price_for_reservations.each do |price_for_reservation|
      if price_for_reservation.possible_overlap_for_hall(from, to, id).positive?
        overlap = 1
        break
      end
    end
    errors.add(:from, :error) if overlap.positive?
    errors.add(:to, :error) if overlap.positive?
  end
end
