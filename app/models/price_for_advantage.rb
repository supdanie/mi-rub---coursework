# frozen_string_literal: true

# This class represents a discount specification for a price advantage.
# It validates the given information (for example filled information in the form).
class PriceForAdvantage < ApplicationRecord
  belongs_to :price_advantage
  belongs_to :price_for_reservation

  validates :discount, presence: true
  validate :discount_correct_percent

  def discount_correct_percent
    return if discount.nil?

    errors.add(:discount, :error) unless discount >= 0 && discount <= 100
  end

  def price_after_discount(price)
    price * (100 - discount) / 100
  end
end
