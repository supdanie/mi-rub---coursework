# frozen_string_literal: true

# This class defines what users can do and what only admins can do.
# This class defines what the current user can do.
class Ability
  include CanCan::Ability

  def initialize(user)
    if user.present?
      can :show, :all
      can :my_reservations, :all
      can :index, :all
      can :reservations, :all
      can :reservation_form, :all
    end

    if user.admin?
      can :manage, :all
      can :new, :all
      can :create, :all
      can :edit, :all
      can :update, :all
      can :destroy, :all
      can :view_users_of_reservation, :all
    elsif user.present?
      can :new, Reservation
      can :create, Reservation, user_id: user.id
      can :edit, user
      can :edit, Reservation, user_id: user.id
      can :update, Reservation, user_id: user.id
      can :destroy, Reservation, user_id: user.id
    end
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
