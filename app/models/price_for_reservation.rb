# frozen_string_literal: true

# This class represents a price specification for given combination of a time range and
# a hall. It validates the given information (for example from the form) and it checks
# whether price for the given hall is not defined for the given time range. It is used
# also for counting the price of a reservation.
class PriceForReservation < ApplicationRecord
  has_many :price_for_advantages, dependent: :destroy

  belongs_to :hall
  belongs_to :time_range

  validates :price, presence: true
  validate :positive_price
  validate :no_overlap_for_hall

  def positive_price
    return if price.nil?

    errors.add(:price, :error) unless price.positive?
  end

  def overlap_for_hall
    prices_for_reservation = PriceForReservation.where(hall_id: hall_id)
    overlap = 0
    prices_for_reservation.each do |price_for_reservation|
      hours = price_for_reservation.hours_in_time_range(time_range.from, time_range.to)
      overlap = 1 if hours.positive?
    end
    overlap
  end

  def possible_overlap_for_hall(from, to, time_range_id)
    prices_for_reservation = PriceForReservation.where(hall_id: hall_id)
    overlap = 0
    prices_for_reservation.each do |price_for_reservation|
      next if price_for_reservation.id == time_range_id

      hours = price_for_reservation.hours_in_time_range(from, to)
      overlap = 1 if hours.positive?
    end
    overlap
  end

  def no_overlap_for_hall
    errors.add(:time_range, :error) if overlap_for_hall.positive?
  end

  def get_price_after_discount(price, user)
    return price if user.price_advantage.nil?

    prices_for_advantage = PriceForAdvantage.where(price_for_reservation_id: id)
    price_for_advantage_for_user = prices_for_advantage.where(price_advantage: user.price_advantage)
    price_after_discount = price
    price_for_advantage_for_user.each do |price_for_user|
      price_after_discount = price_for_user.price_after_discount(price)
    end
    price_after_discount
  end

  def time_in_time_range(from, to)
    from_in_interval = from < time_range.from ? time_range.from : from
    to_in_interval = to > time_range.to ? time_range.to : to
    to_in_interval - from_in_interval
  end

  def hours_in_time_range(from, to)
    return 0 if to <= time_range.from || from >= time_range.to

    time_in_time_range(from, to) / 3600
  end

  def price_in_time_range(from, to, user)
    return 0 if to < time_range.from || from > time_range.to

    original_price = price * time_in_time_range(from, to) / 3600
    get_price_after_discount(original_price, user)
  end
end
