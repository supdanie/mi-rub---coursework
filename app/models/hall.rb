# frozen_string_literal: true

# This class represents a hall. It validates the information about
# a hall (for example the filled information in the form)
# (whether all mandatory information are given, whether the opening hours are valid).
class Hall < ApplicationRecord
  has_many :reservations, dependent: :destroy
  has_many :price_for_reservations, dependent: :destroy

  validates :title, presence: true
  validates :open_from_working_days, presence: true
  validates :open_to_working_days, presence: true
  validates :open_from_weekends, presence: true
  validates :open_to_weekends, presence: true
  validates :default_price_for_reservation, presence: true
  validate :correct_working_days_open_time_range
  validate :correct_weekend_open_time_range
  validate :positive_default_price_for_reservation

  def correct_working_days_open_time_range
    return if open_from_working_days.nil? || open_to_working_days.nil?

    errors.add(:open_from_working_days, :error) unless open_from_working_days < open_to_working_days
    errors.add(:open_to_working_days, :error) unless open_from_working_days < open_to_working_days
  end

  def correct_weekend_open_time_range
    return if open_from_weekends.nil? || open_to_weekends.nil?

    errors.add(:open_from_weekends, :error) unless open_from_weekends < open_to_weekends
    errors.add(:open_to_weekends, :error) unless open_from_weekends < open_to_weekends
  end

  def all_reservations
    Reservation.where(hall_id: id)
  end

  def reservations_in_time_range(from, to, day, reservation_id)
    reservations = []
    all_reservations.each do |reservation|
      next if reservation_id.nil? == false && reservation.id == reservation_id

      reservations.push(reservation) if overlap_reservation(from, to, day, reservation)
    end
    reservations
  end

  def overlap_reservation(from, to, day, reservation)
    overlap_from = from > reservation.from && from < reservation.to
    overlap_to = from <= reservation.from && to > reservation.from
    overlap_day = reservation.date == day

    (overlap_from || overlap_to) && overlap_day
  end

  def reserved?(from, to, day, reservation_id)
    reservations = reservations_in_time_range(from, to, day, reservation_id)
    reservations.count.positive?
  end

  def price_for_time_range(from, to, user)
    all_price_for_reservations = PriceForReservation.where(hall_id: id)
    price_for_time_range = 0
    hours = (to - from) / 3600
    all_price_for_reservations.each do |price_for_reservation|
      price_for_time_range += price_for_reservation.price_in_time_range(from, to, user)
      hours -= price_for_reservation.hours_in_time_range(from, to)
    end

    price_for_time_range + hours * default_price_for_reservation
  end

  def range_in_opening_hours(from, to, day)
    weekend = day.saturday? || day.sunday?
    weekend ? range_for_weekends(from, to) : range_for_working_days(from, to)
  end

  def range_for_weekends(from, to)
    return_value = 0
    return_value += 1 if to > open_to_weekends
    return_value += 2 if from < open_from_weekends
    return_value
  end

  def range_for_working_days(from, to)
    return_value = 0
    return_value += 1 if to > open_to_working_days
    return_value += 2 if from < open_from_working_days
    return_value
  end

  def positive_default_price_for_reservation
    return if default_price_for_reservation.nil?

    unless default_price_for_reservation.positive?
      errors.add(:default_price_for_reservation, :error)
    end
  end
end
