# frozen_string_literal: true

# This class represents a database entity. It is the parent class of all
# entities in the application.
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end
