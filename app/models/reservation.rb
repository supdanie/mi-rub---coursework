# frozen_string_literal: true

# This class represents a reservation and it checks whether the given information
# (for example from the form) are valid. It is also used for counting the price
# for the reservation.
class Reservation < ApplicationRecord
  belongs_to :user
  belongs_to :hall

  validates :from, presence: true
  validates :to, presence: true
  validate :correct_time_range
  validate :hall_not_reserved
  validate :reservation_in_opening_hours
  validate :reservation_in_future

  def correct_time_range
    return if from.nil? || to.nil?

    errors.add(:from, :error) unless from < to
    errors.add(:to, :error) unless from < to
  end

  def hall_not_reserved
    return if hall.nil? || from.nil? || to.nil?

    from_normalized = DateTime.new(2000, 1, 1, from.hour, from.min, from.sec)
    to_normalized = DateTime.new(2000, 1, 1, to.hour, to.min, to.sec)
    errors.add(:from, :error2) if hall.reserved?(from_normalized, to_normalized, date, id)
    errors.add(:to, :error2) if hall.reserved?(from_normalized, to_normalized, date, id)
  end

  def reservation_in_opening_hours
    return if hall.nil? || from.nil? || to.nil?

    from_normalized = DateTime.new(2000, 1, 1, from.hour, from.min, from.sec)
    to_normalized = DateTime.new(2000, 1, 1, to.hour, to.min, to.sec)
    in_opening_hours = hall.range_in_opening_hours(from_normalized, to_normalized, date)
    errors.add(:from, :error3) if in_opening_hours >= 2
    errors.add(:to, :error3) if in_opening_hours.odd?
  end

  def reservation_in_future
    return if from.nil? || to.nil?

    now = DateTime.now
    today = DateTime.new(now.year, now.month, now.day, 0, 0, 0)
    normalized_now = DateTime.new(from.year, from.month, from.day, now.hour, now.min, now.sec)

    in_past = date < today || (date == today && from < normalized_now + 1.0 / 24.0)
    errors.add(:date, :error) if date < today
    errors.add(:from, :error4) if in_past
  end

  def reservations_of_user(user_id)
    Reservation.where(user_id: user_id)
  end

  def price
    hall.price_for_time_range(from, to, user)
  end
end
