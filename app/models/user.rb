# frozen_string_literal: true

# This class represents an user and it is used for working with user accounts.
class User < ApplicationRecord
  has_many :reservations, dependent: :destroy

  belongs_to :price_advantage, optional: true
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable, authentication_keys: [:login]
  validates :username, presence: true, uniqueness: { case_sensitive: false }

  attr_accessor :login
  validate :validate_username

  def login
    @login || username || email
  end

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_h).where(['lower(username) = :value OR lower(email) = :value', { value: login.downcase }]).first
    elsif conditions.key?(:username) || conditions.key?(:email)
      where(conditions.to_h).first
    end
  end

  def admin?
    is_admin
  end

  def validate_username
    errors.add(:username, :invalid) if User.where(email: username).exists?
  end
end
