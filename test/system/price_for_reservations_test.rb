# frozen_string_literal: true

require 'application_system_test_case'

class PriceForReservationsTest < ApplicationSystemTestCase
  setup do
    @price_for_reservation = price_for_reservations(:one)
  end

  test 'visiting the index' do
    visit price_for_reservations_url
    assert_selector 'h1', text: 'Price For Reservations'
  end

  test 'creating a Price for reservation' do
    visit price_for_reservations_url
    click_on 'New Price For Reservation'

    fill_in 'Hall', with: @price_for_reservation.hall_id
    fill_in 'Price', with: @price_for_reservation.price
    fill_in 'Timerange', with: @price_for_reservation.timeRange_id
    click_on 'Create Price for reservation'

    assert_text 'Price for reservation was successfully created'
    click_on 'Back'
  end

  test 'updating a Price for reservation' do
    visit price_for_reservations_url
    click_on 'Edit', match: :first

    fill_in 'Hall', with: @price_for_reservation.hall_id
    fill_in 'Price', with: @price_for_reservation.price
    fill_in 'Timerange', with: @price_for_reservation.timeRange_id
    click_on 'Update Price for reservation'

    assert_text 'Price for reservation was successfully updated'
    click_on 'Back'
  end

  test 'destroying a Price for reservation' do
    visit price_for_reservations_url
    page.accept_confirm do
      click_on 'Destroy', match: :first
    end

    assert_text 'Price for reservation was successfully destroyed'
  end
end
