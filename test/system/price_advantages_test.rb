# frozen_string_literal: true

require 'application_system_test_case'

class PriceAdvantagesTest < ApplicationSystemTestCase
  setup do
    @price_advantage = price_advantages(:one)
  end

  test 'visiting the index' do
    visit price_advantages_url
    assert_selector 'h1', text: 'Price Advantages'
  end

  test 'creating a Price advantage' do
    visit price_advantages_url
    click_on 'New Price Advantage'

    fill_in 'Order', with: @price_advantage.order
    fill_in 'Title', with: @price_advantage.title
    click_on 'Create Price advantage'

    assert_text 'Price advantage was successfully created'
    click_on 'Back'
  end

  test 'updating a Price advantage' do
    visit price_advantages_url
    click_on 'Edit', match: :first

    fill_in 'Order', with: @price_advantage.order
    fill_in 'Title', with: @price_advantage.title
    click_on 'Update Price advantage'

    assert_text 'Price advantage was successfully updated'
    click_on 'Back'
  end

  test 'destroying a Price advantage' do
    visit price_advantages_url
    page.accept_confirm do
      click_on 'Destroy', match: :first
    end

    assert_text 'Price advantage was successfully destroyed'
  end
end
