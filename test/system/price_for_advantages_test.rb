# frozen_string_literal: true

require 'application_system_test_case'

class PriceForAdvantagesTest < ApplicationSystemTestCase
  setup do
    @price_for_advantage = price_for_advantages(:one)
  end

  test 'visiting the index' do
    visit price_for_advantages_url
    assert_selector 'h1', text: 'Price For Advantages'
  end

  test 'creating a Price for advantage' do
    visit price_for_advantages_url
    click_on 'New Price For Advantage'

    fill_in 'Discount', with: @price_for_advantage.discount
    fill_in 'Priceadvantage', with: @price_for_advantage.priceAdvantage_id
    fill_in 'Priceforreservation', with: @price_for_advantage.priceForReservation_id
    click_on 'Create Price for advantage'

    assert_text 'Price for advantage was successfully created'
    click_on 'Back'
  end

  test 'updating a Price for advantage' do
    visit price_for_advantages_url
    click_on 'Edit', match: :first

    fill_in 'Discount', with: @price_for_advantage.discount
    fill_in 'Priceadvantage', with: @price_for_advantage.priceAdvantage_id
    fill_in 'Priceforreservation', with: @price_for_advantage.priceForReservation_id
    click_on 'Update Price for advantage'

    assert_text 'Price for advantage was successfully updated'
    click_on 'Back'
  end

  test 'destroying a Price for advantage' do
    visit price_for_advantages_url
    page.accept_confirm do
      click_on 'Destroy', match: :first
    end

    assert_text 'Price for advantage was successfully destroyed'
  end
end
