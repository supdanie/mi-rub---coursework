# frozen_string_literal: true

require 'test_helper'

class PriceForReservationTest < ActiveSupport::TestCase
  test 'should add valid price_for_reservation' do
    price_for_reservation = PriceForReservation.new
    price_for_reservation.price = 150
    price_for_reservation.hall_id = halls(:one).id
    price_for_reservation.time_range_id = time_ranges(:three).id
    assert price_for_reservation.save, 'Valid price_for_reservation should be added.'
  end

  test 'should not add price_for_reservation without price' do
    price_for_reservation = PriceForReservation.new
    price_for_reservation.hall_id = halls(:one).id
    price_for_reservation.time_range_id = time_ranges(:one).id
    assert_not price_for_reservation.save, 'Price_for_reservation without price should not be added.'
  end

  test 'should not add price_for_reservation with negative price' do
    price_for_reservation = PriceForReservation.new
    price_for_reservation.price = -1
    price_for_reservation.hall_id = halls(:one).id
    price_for_reservation.time_range_id = time_ranges(:one).id
    assert_not price_for_reservation.save, 'Valid price_for_reservation with negative price should not be added.'
  end

  test 'should not redefine price for a time range' do
    price_for_reservation = PriceForReservation.new
    price_for_reservation.price = 150
    price_for_reservation.hall_id = halls(:one).id
    price_for_reservation.time_range_id = time_ranges(:three).id
    assert price_for_reservation.save, 'Valid price_for_reservation should be added.'

    price_for_reservation2 = PriceForReservation.new
    price_for_reservation2.price = 180
    price_for_reservation2.hall_id = halls(:one).id
    price_for_reservation2.time_range_id = time_ranges(:three).id
    assert_not price_for_reservation2.save, 'Price_for_reservation for the given time range and hall should not be added.'
  end
end
