# frozen_string_literal: true

require 'test_helper'

class HallTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test 'should save valid hall' do
    hall = Hall.new
    hall.title = 'Tenisová hala'
    hall.default_price_for_reservation = 150
    hall.open_from_working_days = Time.new(2000, 1, 1, 7, 0, 0)
    hall.open_to_working_days = Time.new(2000, 1, 1, 20, 0, 0)
    hall.open_from_weekends = Time.new(2000, 1, 1, 7, 0, 0)
    hall.open_to_weekends = Time.new(2000, 1, 1, 20, 0, 0)
    hall.sport = 'Tennis'
    hall.description = 'Jedná se o tenisovou halu.'
    assert hall.save, 'Valid hall not saved.'
    assert hall.default_price_for_reservation == 150, 'The default price for reservation should be equal to 150.'
  end

  test 'should save valid hall without description and sport' do
    hall = Hall.new
    hall.title = 'Tenisová hala'
    hall.default_price_for_reservation = 150
    hall.open_from_working_days = Time.new(2000, 1, 1, 7, 0, 0)
    hall.open_to_working_days = Time.new(2000, 1, 1, 20, 0, 0)
    hall.open_from_weekends = Time.new(2000, 1, 1, 7, 0, 0)
    hall.open_to_weekends = Time.new(2000, 1, 1, 20, 0, 0)
    assert hall.save, 'Valid hall not saved.'
    assert hall.default_price_for_reservation == 150, 'The default price for reservation should be equal to 150.'
  end

  test 'should not save hall without title' do
    hall = Hall.new
    hall.default_price_for_reservation = 150
    hall.open_from_working_days = Time.new(2000, 1, 1, 7, 0, 0)
    hall.open_to_working_days = Time.new(2000, 1, 1, 20, 0, 0)
    hall.open_from_weekends = Time.new(2000, 1, 1, 7, 0, 0)
    hall.open_to_weekends = Time.new(2000, 1, 1, 20, 0, 0)
    assert_not hall.save, 'Hall without a title should not be saved.'
  end

  test 'should not save hall without default hour price for reservation' do
    hall = Hall.new
    hall.title = 'Tenisová hala'
    hall.open_from_working_days = Time.new(2000, 1, 1, 7, 0, 0)
    hall.open_to_working_days = Time.new(2000, 1, 1, 20, 0, 0)
    hall.open_from_weekends = Time.new(2000, 1, 1, 7, 0, 0)
    hall.open_to_weekends = Time.new(2000, 1, 1, 20, 0, 0)
    assert_not hall.save, 'Hall without a default price for reservation should not be saved.'
  end

  test 'should not save hall with negative default price for reservation' do
    hall = Hall.new
    hall.title = 'Tenisová hala'
    hall.default_price_for_reservation = -1
    hall.open_from_working_days = Time.new(2000, 1, 1, 7, 0, 0)
    hall.open_to_working_days = Time.new(2000, 1, 1, 20, 0, 0)
    hall.open_from_weekends = Time.new(2000, 1, 1, 7, 0, 0)
    hall.open_to_weekends = Time.new(2000, 1, 1, 20, 0, 0)
    assert_not hall.save, 'Hall with a negative price for reservation should not be saved.'
  end

  test 'should not save hall without opening hours' do
    hall = Hall.new
    hall.title = 'Tenisová hala'
    hall.default_price_for_reservation = 100
    assert_not hall.save, 'Hall without opening hours should not be saved.'
  end

  test 'should not save hall with invalid opening hours' do
    hall = Hall.new
    hall.title = 'Tenisová hala'
    hall.default_price_for_reservation = -1
    hall.open_from_working_days = Time.new(2000, 1, 1, 7, 0, 0)
    hall.open_to_working_days = Time.new(2000, 1, 1, 6, 0, 0)
    hall.open_from_weekends = Time.new(2000, 1, 1, 7, 0, 0)
    hall.open_to_weekends = Time.new(2000, 1, 1, 20, 0, 0)
    assert_not hall.save, 'Hall with invalid opening hours should not be saved.'
  end
end
