# frozen_string_literal: true

require 'test_helper'

class ReservationTest < ActiveSupport::TestCase
  test 'should add valid reservation' do
    reservation = Reservation.new
    reservation.user_id = users(:one).id
    reservation.hall_id = halls(:one).id
    reservation.from = Time.new(2000, 1, 1, 10, 0, 0)
    reservation.to = Time.new(2000, 1, 1, 12, 0, 0)
    reservation.date = DateTime.new(2020, 8, 5, 0, 0, 0)
    assert reservation.save, 'Valid reservation should be added.'
  end

  test 'should not add reservation without from' do
    reservation = Reservation.new
    reservation.user_id = users(:one).id
    reservation.hall_id = halls(:one).id
    reservation.to = Time.new(2000, 1, 1, 12, 0, 0)
    reservation.date = DateTime.new(2020, 8, 5, 0, 0, 0)
    assert_not reservation.save, 'Reservation without from should not be added.'
  end

  test 'should not add reservation without to' do
    reservation = Reservation.new
    reservation.user_id = users(:one).id
    reservation.hall_id = halls(:one).id
    reservation.from = Time.new(2000, 1, 1, 10, 0, 0)
    reservation.date = DateTime.new(2020, 8, 5, 0, 0, 0)
    assert_not reservation.save, 'Reservation without to should not be added.'
  end

  test 'should not add reservation in past' do
    reservation = Reservation.new
    reservation.user_id = users(:one).id
    reservation.hall_id = halls(:one).id
    reservation.from = Time.new(2000, 1, 1, 10, 0, 0)
    reservation.to = Time.new(2000, 1, 1, 12, 0, 0)
    reservation.date = DateTime.now - 1
    assert_not reservation.save, 'Reservation with date in past should not be added.'
  end

  test 'should not add an immediate reservation' do
    reservation = Reservation.new
    reservation.user_id = users(:one).id
    reservation.hall_id = halls(:one).id
    reservation.from = Time.now
    reservation.to = Time.now + 3600
    reservation.date = DateTime.now
    assert_not reservation.save, 'Reservation with date in past should not be added.'
  end

  test 'should not add reservation outside opening hours of the hall' do
    reservation = Reservation.new
    reservation.user_id = users(:one).id
    reservation.hall_id = halls(:one).id
    reservation.from = Time.new(2000, 1, 1, 5, 0, 0)
    reservation.to = Time.new(2000, 1, 1, 7, 0, 0)
    reservation.date = DateTime.now + 1
    assert_not reservation.save, 'Reservation outside the opening hours of the hall should not be added.'
  end

  test 'should not add reservation with invalid time range' do
    reservation = Reservation.new
    reservation.user_id = users(:one).id
    reservation.hall_id = halls(:one).id
    reservation.from = Time.new(2000, 1, 1, 15, 0, 0)
    reservation.to = Time.new(2000, 1, 1, 14, 0, 0)
    reservation.date = DateTime.now + 1
    assert_not reservation.save, 'Reservation with invalid time range should not be added.'
  end

  test 'should not add reservation in time when the hall is reserved' do
    reservation = Reservation.new
    reservation.user_id = users(:two).id
    reservation.hall_id = halls(:one).id
    reservation.from = reservations(:one).from
    reservation.to = reservations(:one).to
    reservation.date = reservations(:one).date
    assert_not reservation.save, 'Reservation in time when someone has reserved the hall should not be added.'
  end

  test 'correct price for user without a price advantage' do
    reservation = Reservation.new
    reservation.user_id = users(:two).id
    reservation.hall_id = halls(:one).id
    reservation.from = Time.new(2000, 1, 1, 11, 0, 0)
    reservation.to = Time.new(2000, 1, 1, 13, 0, 0)
    reservation.date = DateTime.new(2020, 12, 5, 0, 0, 0)
    assert reservation.save, 'Valid reservation should be added.'
    assert reservation.price == 280, 'The price for the reservation should be equal to 280.'
  end

  test 'correct price for user with a price advantage' do
    reservation = Reservation.new
    reservation.user_id = users(:one).id
    reservation.hall_id = halls(:one).id
    reservation.from = Time.new(2000, 1, 1, 11, 0, 0)
    reservation.to = Time.new(2000, 1, 1, 13, 0, 0)
    reservation.date = DateTime.new(2020, 12, 5, 0, 0, 0)
    assert reservation.save, 'Valid reservation should be added.'
    assert reservation.price == 210, 'The price for the reservation should be equal to 210.'
  end
end
