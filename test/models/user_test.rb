# frozen_string_literal: true

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'should add valid user' do
    user = User.new
    user.username = 'supdanie3'
    user.email = 'supdanie3@seznam.cz'
    user.password = 'aaaaaa'
    assert user.save, 'Valid user should be added.'
  end

  test 'should not add user without username' do
    user = User.new
    user.email = 'supdanie3@seznam.cz'
    user.password = 'aaaaaa'
    assert_not user.save, 'User without username should not be added.'
  end

  test 'should not add users with the same username' do
    user = User.new
    user.username = 'supdanie3'
    user.email = 'supdanie3@seznam.cz'
    user.password = 'aaaaaa'
    assert user.save, 'Valid user should be added.'

    user2 = User.new
    user2.username = 'supdanie3'
    user2.email = 'supdanie34@seznam.cz'
    user2.password = 'aaaaaa'
    assert_not user2.save, 'User with the same username as an existing user should not be added.'
  end

  test 'should not add user without password' do
    user = User.new
    user.username = 'supdanie3'
    user.email = 'supdanie3@seznam.cz'
    assert_not user.save, 'User without password should be added.'
  end

  test 'should nota add user with too short password' do
    user = User.new
    user.username = 'supdanie3'
    user.email = 'supdanie3@seznam.cz'
    user.password = 'aaa'
    assert_not user.save, 'User with too short password should not be added.'
  end

  test 'should not add users with the same email address' do
    user = User.new
    user.username = 'supdanie3'
    user.email = 'supdanie3@seznam.cz'
    user.password = 'aaaaaa'
    assert user.save, 'Valid user should be added.'

    user = User.new
    user.username = 'supdanie34'
    user.email = 'supdanie3@seznam.cz'
    user.password = 'aaaaaa'
    assert_not user.save, 'User with the same e-mail address as an existing user should not be added.'
  end

  test 'should not add user with the same username as the email of the first user' do
    user = User.new
    user.username = 'supdanie3'
    user.email = 'supdanie3@seznam.cz'
    user.password = 'aaaaaa'
    assert user.save, 'Valid user should be added.'

    user = User.new
    user.username = 'supdanie3@seznam.cz'
    user.email = 'supdanie34@seznam.cz'
    user.password = 'aaaaaa'
    assert_not user.save, 'User with the same username as the email of the first user should not be added.'
  end
end
