# frozen_string_literal: true

require 'test_helper'

class PriceForAdvantageTest < ActiveSupport::TestCase
  test 'should add correct price_for_advantage' do
    price_for_advantage = PriceForAdvantage.new
    price_for_advantage.discount = 25
    price_for_advantage.price_for_reservation_id = price_for_reservations(:one).id
    price_for_advantage.price_advantage_id = price_advantages(:one).id
    assert price_for_advantage.save, 'Valid price_for_advantage should be saved.'
  end

  test 'should not add price_for_advantage without discount' do
    price_for_advantage = PriceForAdvantage.new
    price_for_advantage.price_for_reservation_id = price_for_reservations(:one).id
    price_for_advantage.price_advantage_id = price_advantages(:one).id
    assert_not price_for_advantage.save, 'Price_for_advantage without discount should not be saved.'
  end

  test 'should not add price_for_reservation with negative discount' do
    price_for_advantage = PriceForAdvantage.new
    price_for_advantage.discount = -25
    price_for_advantage.price_for_reservation_id = price_for_reservations(:one).id
    price_for_advantage.price_advantage_id = price_advantages(:one).id
    assert_not price_for_advantage.save, 'Price_for_advantage with negative discount should not be saved.'
  end

  test 'should not add price_for_reservation with discount higher than 100' do
    price_for_advantage = PriceForAdvantage.new
    price_for_advantage.discount = 125
    price_for_advantage.price_for_reservation_id = price_for_reservations(:one).id
    price_for_advantage.price_advantage_id = price_advantages(:one).id
    assert_not price_for_advantage.save, 'Price_for_advantage with discount higher than 100 should not be saved.'
  end
end
