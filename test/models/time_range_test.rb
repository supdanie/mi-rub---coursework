# frozen_string_literal: true

require 'test_helper'

class TimeRangeTest < ActiveSupport::TestCase
  test 'should add valid time_range' do
    time_range = TimeRange.new
    time_range.from = Time.new(2000, 1, 1, 10, 0, 0)
    time_range.to = Time.new(2000, 1, 1, 12, 0, 0)
    assert time_range.save, 'Valid time_range should be added.'
  end

  test 'should not add time_range without from' do
    time_range = TimeRange.new
    time_range.to = Time.new(2000, 1, 1, 12, 0, 0)
    assert_not time_range.save, 'Time_range without from should not be added.'
  end

  test 'should not add time_range without to' do
    time_range = TimeRange.new
    time_range.from = Time.new(2000, 1, 1, 10, 0, 0)
    assert_not time_range.save, 'Time_range without to should not be added.'
  end

  test 'should not add invalid time_range' do
    time_range = TimeRange.new
    time_range.from = Time.new(2000, 1, 1, 10, 0, 0)
    time_range.to = Time.new(2000, 1, 1, 5, 0, 0)
    assert_not time_range.save, 'Invalid time_range should not be added.'
  end
end
