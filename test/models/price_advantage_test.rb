# frozen_string_literal: true

require 'test_helper'

class PriceAdvantageTest < ActiveSupport::TestCase
  test 'should save valid price_advantage' do
    price_advantage = PriceAdvantage.new
    price_advantage.title = 'Kredit 5000'
    price_advantage.order = 1
    assert price_advantage.save, 'Valid price advantage should be saved.'
  end

  test 'should not save price_advantage without title' do
    price_advantage = PriceAdvantage.new
    price_advantage.order = 1
    assert_not price_advantage.save, 'Price advantage without title should not be saved.'
  end

  test 'should not save price_advantage without order' do
    price_advantage = PriceAdvantage.new
    price_advantage.title = 'Kredit 5000'
    assert_not price_advantage.save, 'Price advantage without order should not be saved.'
  end

  test 'should not save price_advantage with negative order' do
    price_advantage = PriceAdvantage.new
    price_advantage.title = 'Kredit 5000'
    price_advantage.order = -1
    assert_not price_advantage.save, 'Price advantage with negative order should not be saved.'
  end
end
