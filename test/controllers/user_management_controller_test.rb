# frozen_string_literal: true

require 'test_helper'

class UserManagementControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    sign_in users(:one)
  end

  test 'should get index' do
    get '/user_management'
    assert_response :success
  end

  test 'should get edit' do
    get "/user_management/edit/#{users(:one).id}"
    assert_response :success
  end
end
