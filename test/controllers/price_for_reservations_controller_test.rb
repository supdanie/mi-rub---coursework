# frozen_string_literal: true

require 'test_helper'

class PriceForReservationsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @price_for_reservation = price_for_reservations(:one)
    sign_in users(:one)
  end

  test 'should get index' do
    get price_for_reservations_url
    assert_response :success
  end

  test 'should get new' do
    get new_price_for_reservation_url
    assert_response :success
  end

  test 'should create price_for_reservation' do
    assert_difference('PriceForReservation.count') do
      post price_for_reservations_url, params: { price_for_reservation: { hall_id: @price_for_reservation.hall_id, price: @price_for_reservation.price, time_range_id: time_ranges(:three).id } }
    end

    assert_redirected_to price_for_reservation_url(PriceForReservation.last)
  end

  test 'should not create price_for_reservation without price' do
    assert_difference('PriceForReservation.count', 0) do
      post price_for_reservations_url, params: { price_for_reservation: { hall_id: @price_for_reservation.hall_id, time_range_id: time_ranges(:three).id } }
    end

    assert_response :success
  end

  test 'should not create price_for_reservatoin with negative price' do
    assert_difference('PriceForReservation.count', 0) do
      post price_for_reservations_url, params: { price_for_reservation: { hall_id: @price_for_reservation.hall_id, price: -5, time_range_id: time_ranges(:three).id } }
    end

    assert_response :success
  end

  test 'should not create price_for_reservation for overlaping time_range for the given hall' do
    assert_difference('PriceForReservation.count', 0) do
      post price_for_reservations_url, params: { price_for_reservation: { hall_id: @price_for_reservation.hall_id, price: @price_for_reservation.price, time_range_id: time_ranges(:one).id } }
    end

    assert_response :success
  end

  test 'should show price_for_reservation' do
    get price_for_reservation_url(@price_for_reservation)
    assert_response :success
  end

  test 'should get edit' do
    get edit_price_for_reservation_url(@price_for_reservation)
    assert_response :success
  end

  test 'should update price_for_reservation' do
    patch price_for_reservation_url(@price_for_reservation), params: { price_for_reservation: { hall_id: @price_for_reservation.hall_id, price: @price_for_reservation.price, time_range_id: time_ranges(:three).id } }
    assert_redirected_to price_for_reservation_url(@price_for_reservation)
  end

  test 'should not update price_for_reservation with negative price' do
    patch price_for_reservation_url(@price_for_reservation), params: { price_for_reservation: { hall_id: @price_for_reservation.hall_id, price: -1, time_range_id: time_ranges(:three).id } }
    assert_response :success
  end

  test 'should not update price_for_reservation with overlaping time range' do
    patch price_for_reservation_url(@price_for_reservation), params: { price_for_reservation: { hall_id: @price_for_reservation.hall_id, price: @price_for_reservation.price, time_range_id: time_ranges(:one).id } }
    assert_response :success
  end

  test 'should destroy price_for_reservation' do
    assert_difference('PriceForReservation.count', -1) do
      delete price_for_reservation_url(@price_for_reservation)
    end

    assert_redirected_to price_for_reservations_url
  end
end
