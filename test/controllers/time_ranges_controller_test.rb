# frozen_string_literal: true

require 'test_helper'

class TimeRangesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @time_range = time_ranges(:one)
    sign_in users(:one)
  end

  test 'should get index' do
    get time_ranges_url
    assert_response :success
  end

  test 'should get new' do
    get new_time_range_url
    assert_response :success
  end

  test 'should create time_range' do
    assert_difference('TimeRange.count') do
      post time_ranges_url, params: { time_range: { from: @time_range.from, to: @time_range.to } }
    end

    assert_redirected_to time_range_url(TimeRange.last)
  end

  test 'should not create time_range without to' do
    assert_difference('TimeRange.count', 0) do
      post time_ranges_url, params: { time_range: { from: @time_range.from } }
    end
  end

  test 'should not create time_range without from' do
    assert_difference('TimeRange.count', 0) do
      post time_ranges_url, params: { time_range: { to: @time_range.to } }
    end
  end

  test 'should not create time_range with invalid from and to' do
    assert_difference('TimeRange.count', 0) do
      post time_ranges_url, params: { time_range: { to: @time_range.from, from: @time_range.to } }
    end
  end

  test 'should show time_range' do
    get time_range_url(@time_range)
    assert_response :success
  end

  test 'should get edit' do
    get edit_time_range_url(@time_range)
    assert_response :success
  end

  test 'should update time_range' do
    patch time_range_url(@time_range), params: { time_range: { from: @time_range.from, to: @time_range.to } }
    assert_redirected_to time_range_url(@time_range)
  end

  test 'should not update time_range with invalid from and to' do
    patch time_range_url(@time_range), params: { time_range: { from: @time_range.to, to: @time_range.from } }
    assert_response :success
  end

  test 'should destroy time_range' do
    assert_difference('TimeRange.count', -1) do
      delete time_range_url(@time_range)
    end

    assert_redirected_to time_ranges_url
  end
end
