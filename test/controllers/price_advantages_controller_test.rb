# frozen_string_literal: true

require 'test_helper'

class PriceAdvantagesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @price_advantage = price_advantages(:one)
    sign_in users(:one)
  end

  test 'should get index' do
    get price_advantages_url
    assert_response :success
  end

  test 'should get new' do
    get new_price_advantage_url
    assert_response :success
  end

  test 'shoud not create price_advantage without title' do
    assert_difference('PriceAdvantage.count', 0) do
      post price_advantages_url, params: { price_advantage: { order: @price_advantage.order } }
    end

    assert_response :success
  end

  test 'should not create price_advantage without order' do
    assert_difference('PriceAdvantage.count', 0) do
      post price_advantages_url, params: { price_advantage: { title: @price_advantage.title } }
    end

    assert_response :success
  end

  test 'should not create price_advantage with negative order' do
    assert_difference('PriceAdvantage.count', 0) do
      post price_advantages_url, params: { price_advantage: { order: -3, title: @price_advantage.title } }
    end

    assert_response :success
  end

  test 'should create price_advantage' do
    assert_difference('PriceAdvantage.count') do
      post price_advantages_url, params: { price_advantage: { order: @price_advantage.order, title: @price_advantage.title } }
    end

    assert_redirected_to price_advantage_url(PriceAdvantage.last)
  end

  test 'should show price_advantage' do
    get price_advantage_url(@price_advantage)
    assert_response :success
  end

  test 'should get edit' do
    get edit_price_advantage_url(@price_advantage)
    assert_response :success
  end

  test 'should update price_advantage' do
    patch price_advantage_url(@price_advantage), params: { price_advantage: { order: @price_advantage.order, title: @price_advantage.title } }
    assert_redirected_to price_advantage_url(@price_advantage)
  end

  test 'should not update price_advantage with incorrect order' do
    patch price_advantage_url(@price_advantage), params: { price_advantage: { order: -4, title: @price_advantage.title } }
    assert_response :success
  end

  test 'should destroy price_advantage' do
    assert_difference('PriceAdvantage.count', -1) do
      delete price_advantage_url(@price_advantage)
    end

    assert_redirected_to price_advantages_url
  end
end
