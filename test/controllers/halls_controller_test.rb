# frozen_string_literal: true

require 'test_helper'
require 'less-rails'
require 'therubyracer'
require 'twitter-bootstrap-rails'

class HallsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @hall = halls(:one)
    sign_in users(:one)
  end

  test 'should get index' do
    get halls_url
    assert_response :success
  end

  test 'should get new' do
    get new_hall_url
    assert_response :success
  end

  test 'should not create hall without title' do
    assert_difference('Hall.count', 0) do
      post halls_url, params: { hall: { description: @hall.description, open_from_weekends: @hall.open_from_weekends, open_from_working_days: @hall.open_from_working_days, open_to_weekends: @hall.open_to_weekends, open_to_working_days: @hall.open_to_working_days, default_price_for_reservation: @hall.default_price_for_reservation, sport: @hall.sport } }
    end

    assert_response :success
  end

  test 'should not create hall without default hour price for reservation' do
    assert_difference('Hall.count', 0) do
      post halls_url, params: { hall: { description: @hall.description, open_from_weekends: @hall.open_from_weekends, open_from_working_days: @hall.open_from_working_days, open_to_weekends: @hall.open_to_weekends, open_to_working_days: @hall.open_to_working_days, title: @hall.title, sport: @hall.sport } }
    end

    assert_response :success
  end

  test 'should not create hall with incorrect opening hours' do
    assert_difference('Hall.count', 0) do
      post halls_url, params: { hall: { description: @hall.description, open_from_weekends: @hall.open_to_weekends, open_from_working_days: @hall.open_to_working_days, open_to_weekends: @hall.open_from_weekends, open_to_working_days: @hall.open_to_working_days, default_price_for_reservation: @hall.default_price_for_reservation, title: @hall.title, sport: @hall.sport } }
    end

    assert_response :success
  end

  test 'should not create hall without opening hours' do
    assert_difference('Hall.count', 0) do
      post halls_url, params: { hall: { description: @hall.description, default_price_for_reservation: @hall.default_price_for_reservation, title: @hall.title, sport: @hall.sport } }
    end

    assert_response :success
  end

  test 'should create hall without description' do
    assert_difference('Hall.count') do
      post halls_url, params: { hall: { open_from_weekends: @hall.open_from_weekends, open_from_working_days: @hall.open_from_working_days, open_to_weekends: @hall.open_to_weekends, open_to_working_days: @hall.open_to_working_days, default_price_for_reservation: @hall.default_price_for_reservation, sport: @hall.sport, title: @hall.title } }
    end

    assert_redirected_to hall_url(Hall.last)
  end

  test 'should create hall without sport' do
    assert_difference('Hall.count') do
      post halls_url, params: { hall: { description: @hall.description, open_from_weekends: @hall.open_from_weekends, open_from_working_days: @hall.open_from_working_days, open_to_weekends: @hall.open_to_weekends, open_to_working_days: @hall.open_to_working_days, default_price_for_reservation: @hall.default_price_for_reservation, title: @hall.title } }
    end

    assert_redirected_to hall_url(Hall.last)
  end

  test 'should create hall' do
    assert_difference('Hall.count') do
      post halls_url, params: { hall: { description: @hall.description, open_from_weekends: @hall.open_from_weekends, open_from_working_days: @hall.open_from_working_days, open_to_weekends: @hall.open_to_weekends, open_to_working_days: @hall.open_to_working_days, default_price_for_reservation: @hall.default_price_for_reservation, sport: @hall.sport, title: @hall.title } }
    end

    assert_redirected_to hall_url(Hall.last)
  end

  test 'should show hall' do
    get hall_url(@hall)
    assert_response :success
  end

  test 'should get edit' do
    get edit_hall_url(@hall)
    assert_response :success
  end

  test 'should update hall' do
    patch hall_url(@hall), params: { hall: { description: @hall.description, open_from_weekends: @hall.open_from_weekends, open_from_working_days: @hall.open_from_working_days, open_to_weekends: @hall.open_to_weekends, open_to_working_days: @hall.open_to_working_days, default_price_for_reservation: @hall.default_price_for_reservation, sport: @hall.sport, title: @hall.title } }
    assert_redirected_to hall_url(@hall)
  end

  test 'should not update hall with incorrect opening hours' do
    patch hall_url(@hall), params: { hall: { description: @hall.description, open_from_weekends: @hall.open_to_weekends, open_from_working_days: @hall.open_from_working_days, open_to_weekends: @hall.open_from_weekends, open_to_working_days: @hall.open_from_working_days, default_price_for_reservation: @hall.default_price_for_reservation, sport: @hall.sport, title: @hall.title } }
    assert_response :success
  end

  test 'should destroy hall' do
    assert_difference('Hall.count', -1) do
      delete hall_url(@hall)
    end

    assert_redirected_to halls_url
  end
end
