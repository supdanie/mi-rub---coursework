# frozen_string_literal: true

require 'test_helper'

class PriceForAdvantagesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @price_for_advantage = price_for_advantages(:one)
    sign_in users(:one)
  end

  test 'should get index' do
    get price_for_advantages_url
    assert_response :success
  end

  test 'should get new' do
    get new_price_for_advantage_url
    assert_response :success
  end

  test 'should create price_for_advantage' do
    assert_difference('PriceForAdvantage.count') do
      post price_for_advantages_url, params: { price_for_advantage: { discount: @price_for_advantage.discount, price_advantage_id: @price_for_advantage.price_advantage_id, price_for_reservation_id: @price_for_advantage.price_for_reservation_id } }
    end

    assert_redirected_to price_for_advantage_url(PriceForAdvantage.last)
  end

  test 'should not create price_for_advantage without discount' do
    assert_difference('PriceForAdvantage.count', 0) do
      post price_for_advantages_url, params: { price_for_advantage: { price_advantage_id: @price_for_advantage.price_advantage_id, price_for_reservation_id: @price_for_advantage.price_for_reservation_id } }
    end

    assert_response :success
  end

  test 'should not create price_for_advantage with negative discount' do
    assert_difference('PriceForAdvantage.count', 0) do
      post price_for_advantages_url, params: { price_for_advantage: { discount: -5, price_advantage_id: @price_for_advantage.price_advantage_id, price_for_reservation_id: @price_for_advantage.price_for_reservation_id } }
    end

    assert_response :success
  end

  test 'should not create price_for_advantage with discount higher than 100' do
    assert_difference('PriceForAdvantage.count', 0) do
      post price_for_advantages_url, params: { price_for_advantage: { discount: 128, price_advantage_id: @price_for_advantage.price_advantage_id, price_for_reservation_id: @price_for_advantage.price_for_reservation_id } }
    end

    assert_response :success
  end

  test 'should show price_for_advantage' do
    get price_for_advantage_url(@price_for_advantage)
    assert_response :success
  end

  test 'should get edit' do
    get edit_price_for_advantage_url(@price_for_advantage)
    assert_response :success
  end

  test 'should update price_for_advantage' do
    patch price_for_advantage_url(@price_for_advantage), params: { price_for_advantage: { discount: @price_for_advantage.discount, price_advantage_id: @price_for_advantage.price_advantage_id, price_for_reservation_id: @price_for_advantage.price_for_reservation_id } }
    assert_redirected_to price_for_advantage_url(@price_for_advantage)
  end

  test 'should not update price_for_advantage with negative discount' do
    patch price_for_advantage_url(@price_for_advantage), params: { price_for_advantage: { discount: -1, price_advantage_id: @price_for_advantage.price_advantage_id, price_for_reservation_id: @price_for_advantage.price_for_reservation_id } }
    assert_response :success
  end

  test 'should not update price_for_advantage with discount higher than 100' do
    patch price_for_advantage_url(@price_for_advantage), params: { price_for_advantage: { discount: 101, price_advantage_id: @price_for_advantage.price_advantage_id, price_for_reservation_id: @price_for_advantage.price_for_reservation_id } }
    assert_response :success
  end

  test 'should destroy price_for_advantage' do
    assert_difference('PriceForAdvantage.count', -1) do
      delete price_for_advantage_url(@price_for_advantage)
    end

    assert_redirected_to price_for_advantages_url
  end
end
