# frozen_string_literal: true

require 'test_helper'

class ReservationsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @reservation = reservations(:one)
    sign_in users(:one)
  end

  test 'should get index' do
    get reservations_url
    assert_response :success
  end

  test 'should get new' do
    get new_reservation_url
    assert_response :success
  end

  test 'should create reservation' do
    assert_difference('Reservation.count') do
      post reservations_url, params: { reservation: { sport: @reservation.sport, date: @reservation.date + 1, from: @reservation.from, hall_id: @reservation.hall_id, to: @reservation.to, user_id: @reservation.user_id } }
    end

    assert_redirected_to reservation_url(Reservation.last)
  end

  test 'should create reservation as the current user with correct price' do
    assert_difference('Reservation.count') do
      post reservations_url, params: { reservation: { sport: @reservation.sport, date: @reservation.date + 1, from: @reservation.from, hall_id: @reservation.hall_id, to: @reservation.to, user_id: users(:one).id } }
    end

    assert_redirected_to reservation_url(Reservation.last)
    assert Reservation.last.price == 105, 'The price for the reservation should be equal to 105.'
  end

  test 'should create reservation as the second user with correct price' do
    assert_difference('Reservation.count') do
      post reservations_url, params: { reservation: { sport: @reservation.sport, date: @reservation.date + 1, from: @reservation.from, hall_id: @reservation.hall_id, to: @reservation.to, user_id: users(:two).id } }
    end

    assert_redirected_to reservation_url(Reservation.last)
    assert Reservation.last.price == 140, 'The price for the reservation should be equal to 140.'
  end

  test 'should not create reservation without time range' do
    assert_difference('Reservation.count', 0) do
      post reservations_url, params: { reservation: { sport: @reservation.sport, date: @reservation.date + 1, hall_id: @reservation.hall_id, user_id: @reservation.user_id } }
    end

    assert_response :success
  end

  test 'should not create reservation with incorrect time range' do
    assert_difference('Reservation.count', 0) do
      post reservations_url, params: { reservation: { sport: @reservation.sport, from: @reservation.to, to: @reservation.from, date: @reservation.date + 1, hall_id: @reservation.hall_id, user_id: @reservation.user_id } }
    end

    assert_response :success
  end

  test 'should not create reservation when someone else has reserved the hall' do
    assert_difference('Reservation.count') do
      post reservations_url, params: { reservation: { sport: @reservation.sport, date: @reservation.date + 1, from: @reservation.from, hall_id: @reservation.hall_id, to: @reservation.to, user_id: users(:one).id } }
    end

    assert_redirected_to reservation_url(Reservation.last)

    assert_difference('Reservation.count', 0) do
      post reservations_url, params: { reservation: { sport: @reservation.sport, date: @reservation.date + 1, from: @reservation.from, hall_id: @reservation.hall_id, to: @reservation.to, user_id: users(:two).id } }
    end

    assert_response :success
  end

  test 'should show reservation' do
    get reservation_url(@reservation)
    assert_response :success
  end

  test 'should get edit' do
    get edit_reservation_url(@reservation)
    assert_response :success
  end

  test 'should update reservation' do
    patch reservation_url(@reservation), params: { reservation: { sport: @reservation.sport, date: @reservation.date, from: @reservation.from, hall_id: @reservation.hall_id, to: @reservation.to, user_id: @reservation.user_id } }
    assert_redirected_to reservation_url(@reservation)
  end

  test 'should not update reservation with incorrect time range' do
    patch reservation_url(@reservation), params: { reservation: { sport: @reservation.sport, date: @reservation.date, from: @reservation.from, hall_id: @reservation.hall_id, to: @reservation.from - 1.0 / 24.0, user_id: @reservation.user_id } }
    assert_response :success
  end

  test 'should not update reservation when someone else has reserved the given hall' do
    patch reservation_url(@reservation), params: { reservation: { sport: @reservation.sport, date: reservations(:two).date, from: reservations(:two).from, hall_id: reservations(:two).hall_id, to: reservations(:two).to, user_id: @reservation.user_id } }
    assert_response :success
  end

  test 'should destroy reservation' do
    assert_difference('Reservation.count', -1) do
      delete reservation_url(@reservation)
    end

    assert_redirected_to reservations_url
  end
end
