# frozen_string_literal: true

Rails.application.routes.draw do
  get 'user_management', to: 'user_management#index'
  get 'user_management/edit/:id', to: 'user_management#edit', as: :user
  patch 'user_management/edit/:id', to: 'user_management#create'
  resources :price_for_advantages
  resources :price_for_reservations
  resources :price_advantages
  resources :reservations
  resources :halls
  resources :time_ranges
  devise_for :users
  get 'home/index'
  root 'home#index'
  get 'halls/:id/reservations', to: 'halls#reservation_form'
  post 'halls/:id/reservations', to: 'halls#reservations'
  get 'my_reservations', to: 'reservations#my_reservations'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
